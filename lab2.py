#!/usr/bin/python                                                                            
                                                                                             
from mininet.topo import Topo
from mininet.net import Mininet
from mininet.util import dumpNodeConnections
from mininet.log import setLogLevel


class FatTreeTopo(Topo):
    "A k-fat-tree topo"
    # build a single pod, arguments are pod number and size of tree (k)
    # returns a list of all components it creates
    def buildPod(self, pod, k = 2):
        # setup arrays for each component in the pod
        aggs = []
        edges = []
        hosts = []
        # create the agg and edge switches
        
        for n in range(k/2):
            aggs.append(self.addSwitch('agSw%s%s'%(pod,n)))
            edges.append(self.addSwitch('edSw%s%s'%(pod,n)))

        # link the agg and edge switches
        for ag in aggs:
            for ed in edges:
                self.addLink(ag,ed)

        hnum = 0 
        # create the hosts and link them to their edge
        for edge in edges:
            for h in range(k/2):
                host = self.addHost('h%s%s'%(pod,hnum))
                hnum = hnum+1
                self.addLink(host,edge)
                hosts.append(host)

        return aggs


    def build(self, k=2):
        # build the core routers
        core = []
        aggs = []
        for n in range(k):
            core.append(self.addSwitch('crSW%s'%(n)))
            aggs.append( self.buildPod(n, k))
        # connect the core routers and aggs
        for c in range(k):
            for pod in range(k):
                self.addLink(core[c],aggs[pod][c/2])







def simpleTest():
    "Create and test a simple network"
    # k must be even
    topo = FatTreeTopo(k=4)
    net = Mininet(topo)
    net.start()
    print "Dumping host connections"
    dumpNodeConnections(net.hosts)
    print "Testing network connectivity"
    net.pingAll()
    net.stop()

if __name__ == '__main__':
    # Tell mininet to print useful information
    setLogLevel('info')
    simpleTest()